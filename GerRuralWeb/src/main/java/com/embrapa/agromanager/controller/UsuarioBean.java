package com.embrapa.agromanager.controller;

import com.embrapa.agromanager.dao.DaoUsuario;
import com.embrapa.agromanager.model.Usuario;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "usuarioBean") //define como a view vai poder usar ela, chamando usuarioBean.
@ViewScoped //define que essa classe somente ficará viva enquanto o usuário estiver na página que ela esta sendo chamada.
//para durar enquanto sessão usar SessionScoped. 
public class UsuarioBean implements Serializable {

    @EJB // notação do Enterprise JavaBeans (EJB) que é um componente da plataforma JEE que roda em um container de um servidor de aplicação. https://pt.wikipedia.org/wiki/Enterprise_JavaBeans
    DaoUsuario daoUsuario; // Injeção da classe daoUsuario no container. Como se estivesse dando um "new" nessa instância
    private Usuario usuarioNovo;

    private boolean novo, detalheUsuario, editarUsuario;

    public UsuarioBean() {
        System.out.println("Construtor sendo executado");
        usuarioNovo = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("usuarioselecionado");
    }

    @PostConstruct
    public void init() {
        System.out.println("Init Sendo Executado");

        if (usuarioNovo == null) {
            novo = true;
            detalheUsuario = false;
            editarUsuario = false;
            usuarioNovo = new Usuario();
        } else {
            usuarioNovo = daoUsuario.buscarUsuarioPorId(usuarioNovo.getId()); //para ter certeza q esta com usuario certo, caso esteva tentado fazer 2 operações em telas diferentes sobre o mesmo usuario
            if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalheusuario") != null) {
                detalheUsuario = (Boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("detalheusuario");

            } else if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editarusuario") != null) {
                editarUsuario = (Boolean) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("editarusuario");

            }
        }
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("usuarioselecionado");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("detalheusuario");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().remove("editarusuario");
    }

    public String converteHexa(String s) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(s.getBytes(), 0, s.length());
        return (String) new BigInteger(1, m.digest()).toString(16);

    }

    public void salvarUsuario() throws NoSuchAlgorithmException {

        System.out.println("Entrou no salvar usuario");
        usuarioNovo.setSenha(converteHexa(usuarioNovo.getSenha()));
        usuarioNovo.setTipoUser(true);
        usuarioNovo.setStatus(true);
        usuarioNovo = daoUsuario.criarUsuario(usuarioNovo);
        if (usuarioNovo != null) {
            System.out.println("Cadastrado com sucesso");
            usuarioNovo = new Usuario();//preparando novo usuario
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("objetivo", usuarioNovo); // colocando algum valor na session
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("dashboard.xhtml"); // redirecionado para uma página

            } catch (IOException ex) {
                Logger.getLogger(UsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("Problemas ao cadastrar");
        }
    }

    public Usuario getUsuarioNovo() {
        return usuarioNovo;
    }

    public void setUsuarioNovo(Usuario usuarioNovo) {
        this.usuarioNovo = usuarioNovo;
    }

    public String identificarPerfil(Usuario usuarioSelecionado) {
        if (usuarioSelecionado != null) {
            if (usuarioSelecionado.isTipoUser()) {
                return "Produtor";
            } else {
                return "Administrador";
            }
        }
        return "-";
    }

    public String identificarStatus(Usuario usuarioSelecionado) {
        if (usuarioSelecionado != null) {
            if (usuarioSelecionado.isStatus()) {
                return "Ativo";
            } else {
                return "Desativado";
            }
        }
        return "-";
    }

    public DaoUsuario getDaoUsuario() {
        return daoUsuario;
    }

    public void setDaoUsuario(DaoUsuario daoUsuario) {
        this.daoUsuario = daoUsuario;
    }

    public boolean isNovo() {
        return novo;
    }

    public void setNovo(boolean novo) {
        this.novo = novo;
    }

    public boolean isDetalheUsuario() {
        return detalheUsuario;
    }

    public void setDetalheUsuario(boolean detalheUsuario) {
        this.detalheUsuario = detalheUsuario;
    }

    public boolean isEditarUsuario() {
        return editarUsuario;
    }

    public void setEditarUsuario(boolean editarUsuario) {
        this.editarUsuario = editarUsuario;
    }

    public String logarUsuario() throws NoSuchAlgorithmException {
        Integer resultado = daoUsuario.validaUsuario(usuarioNovo.getCpf(), converteHexa(usuarioNovo.getSenha()));
        if (resultado != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Logado"));
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("usuario_logado", resultado); // colocando algum valor na session. Olhar classe DetalheUsuarioBean para ver como pego esse objeto
            return "/dashboard.xhtml?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Falha no acesso"));
            return "/login.xhtml?faces-redirect=true";
        }

    }

    public String desativaUsuario() {
        usuarioNovo.setStatus(false);
        daoUsuario.updateUsuario(usuarioNovo);
        System.out.println("Usuario desativado");
        return "/listaUsuario.xhtml?faces-redirect=true";

    }
    
    public String logout(){
        return "";
    }

}
