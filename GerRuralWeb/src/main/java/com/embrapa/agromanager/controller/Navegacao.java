/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.embrapa.agromanager.controller;


import com.embrapa.agromanager.dao.DaoUsuario;
import com.embrapa.agromanager.model.Usuario;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

/**
 *
 * @author gomes
 */
@Named(value = "navegacao")
@ApplicationScoped
public class Navegacao {
     @EJB
     DaoUsuario daoUsuario;
    

 
    /**
     * Creates a new instance of Navegacao
     */
    public Navegacao() {
        
    }
    
     public String resultid(){
     Integer id = (Integer) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("usuario_logado"); 
     if(id!=null){
        Usuario usuarioNovo = daoUsuario.buscarUsuarioPorId(id);
        return usuarioNovo.getNome();
     }else
         return null;
    }
    public static String irLogin(){
    
        NavigationHandler naviHandler = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        naviHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/login.xhtml?faces-redirect=true");
        return "/login.xhtml?faces-redirect=true";
    
    }
    
    public static String irCadastroUsuario(){
    
        NavigationHandler naviHandler = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        naviHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/cadastroUsuario.xhtml?faces-redirect=true");
        return "/cadastroUsuario.xhtml?faces-redirect=true";
    
    }
    
    public static String irDash(){
    
        NavigationHandler naviHandler = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        naviHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/dashboard.xhtml?faces-redirect=true");
        return "/dashboard.xhtml?faces-redirect=true";
    
    }
    
  public static String irIndex() {
 
        NavigationHandler naviHandler = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        naviHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/index.xhtml?faces-redirect=true");
        return "/index.xhtml?faces-redirect=true";
    }

    public static String irListaUsuario() {
        NavigationHandler naviHandler = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        naviHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/listaUsuarios.xhtml?faces-redirect=true");
        return "/listaUsuarios.xhtml?faces-redirect=true";
    }

    
     public static String irUsuario() {
        NavigationHandler naviHandler = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        naviHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/formularioUsuario.xhtml?faces-redirect=true");
        return "/formularioUsuario.xhtml?faces-redirect=true";
    }
     

        public static String irListaEmpresa() {
        NavigationHandler naviHandler = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        naviHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/listaEmpresa.xhtml?faces-redirect=true");
        return "/listaEmpresa.xhtml?faces-redirect=true";
    }

     public static String irEmpresa() {
        NavigationHandler naviHandler = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        naviHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/formularioEmpresa.xhtml?faces-redirect=true");
       
        return "/formularioEmpresa.xhtml?faces-redirect=true";
    } 
     
     public static String irFilial() {
        NavigationHandler naviHandler = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        naviHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/formularioFilial.xhtml?faces-redirect=true");
       
        return "/formularioFilial.xhtml?faces-redirect=true";
    } 

    
    
}
