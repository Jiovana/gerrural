/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.embrapa.agromanager.dao;

import com.embrapa.agromanager.model.Usuario;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author gomes
 */
@Stateful
@LocalBean
public class DaoUsuario extends Dao<Usuario, Integer>{
     public DaoUsuario() {
        super();
    }

    /**
     * Método para criar novo usuario
     *
     * @param usuario
     * @return
     */
    public Usuario criarUsuario(Usuario usuario) {
        return super.salvar(usuario);
    }

    /**
     * Método para atualizar usuario existente
     *
     * @param usuario
     * @return
     */
    public Usuario updateUsuario(Usuario usuario) {
        return super.update(usuario);
    }

    /**
     * Método para buscar usuario por ID
     *
     * @param idUsuario
     * @return
     */
    public Usuario buscarUsuarioPorId(Integer idUsuario) {
        return (Usuario) super.buscarObjeto(idUsuario, Usuario.class);
    }
    
   
    
     public Integer validaUsuario(String cpf, String senha) {
         
        EntityManager manager = getEntityManager();
        Query query = manager.createQuery("SELECT user FROM Usuario AS user WHERE user.cpf ='"+cpf+"'AND user.senha='"+senha+"'", Usuario.class);
        Usuario usuarioBanco;
        try {       
            usuarioBanco = (Usuario) query.getSingleResult();
            if(usuarioBanco == null){
                return null;
            }else            
               return usuarioBanco.getId();
        } catch (NoResultException nre) {
            System.out.println("Login error -->" + nre.getMessage());
            return null;
        }finally {
            manager.flush();
            manager.clear();
        }
    }
     

    /**
     * Método para buscar todos usuarios
     *
     * @return
     */
    public List<Usuario> buscarUsuarios() {
        return (List<Usuario>) super.buscarObjetos(Usuario.class);
    }

    /**
     * Método para excluir usuario por ID
     *
     * @param idUser
     * @return
     */
    public boolean excluirUsuario(Integer idUser) {
        return super.excluir(idUser, Usuario.class);
    }
    
    
}
