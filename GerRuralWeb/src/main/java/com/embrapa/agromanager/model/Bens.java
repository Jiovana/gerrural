/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.embrapa.agromanager.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gomes
 */
@Entity
@Table(name = "bens")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bens.findAll", query = "SELECT b FROM Bens b")
    , @NamedQuery(name = "Bens.findById", query = "SELECT b FROM Bens b WHERE b.id = :id")
    , @NamedQuery(name = "Bens.findByNome", query = "SELECT b FROM Bens b WHERE b.nome = :nome")
    , @NamedQuery(name = "Bens.findByUnid", query = "SELECT b FROM Bens b WHERE b.unid = :unid")
    , @NamedQuery(name = "Bens.findByQtd", query = "SELECT b FROM Bens b WHERE b.qtd = :qtd")
    , @NamedQuery(name = "Bens.findByVidaUtil", query = "SELECT b FROM Bens b WHERE b.vidaUtil = :vidaUtil")
    , @NamedQuery(name = "Bens.findByAnoFabr", query = "SELECT b FROM Bens b WHERE b.anoFabr = :anoFabr")
    , @NamedQuery(name = "Bens.findByValorUn", query = "SELECT b FROM Bens b WHERE b.valorUn = :valorUn")
    , @NamedQuery(name = "Bens.findByValorRes", query = "SELECT b FROM Bens b WHERE b.valorRes = :valorRes")
    , @NamedQuery(name = "Bens.findByTaxaMnt", query = "SELECT b FROM Bens b WHERE b.taxaMnt = :taxaMnt")
    , @NamedQuery(name = "Bens.findByTipob", query = "SELECT b FROM Bens b WHERE b.tipob = :tipob")
    , @NamedQuery(name = "Bens.findByTipoAgro", query = "SELECT b FROM Bens b WHERE b.tipoAgro = :tipoAgro")})
public class Bens implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "unid")
    private String unid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "qtd")
    private int qtd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vida_util")
    private int vidaUtil;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ano_fabr")
    @Temporal(TemporalType.DATE)
    private Date anoFabr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor_un")
    private double valorUn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor_res")
    private float valorRes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxa_mnt")
    private float taxaMnt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipob")
    private Character tipob;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo_agro")
    private Character tipoAgro;
    @JoinColumn(name = "prop_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Propriedade propId;

    public Bens() {
    }

    public Bens(Integer id) {
        this.id = id;
    }

    public Bens(Integer id, String nome, String unid, int qtd, int vidaUtil, Date anoFabr, double valorUn, float valorRes, float taxaMnt, Character tipob, Character tipoAgro) {
        this.id = id;
        this.nome = nome;
        this.unid = unid;
        this.qtd = qtd;
        this.vidaUtil = vidaUtil;
        this.anoFabr = anoFabr;
        this.valorUn = valorUn;
        this.valorRes = valorRes;
        this.taxaMnt = taxaMnt;
        this.tipob = tipob;
        this.tipoAgro = tipoAgro;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUnid() {
        return unid;
    }

    public void setUnid(String unid) {
        this.unid = unid;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public int getVidaUtil() {
        return vidaUtil;
    }

    public void setVidaUtil(int vidaUtil) {
        this.vidaUtil = vidaUtil;
    }

    public Date getAnoFabr() {
        return anoFabr;
    }

    public void setAnoFabr(Date anoFabr) {
        this.anoFabr = anoFabr;
    }

    public double getValorUn() {
        return valorUn;
    }

    public void setValorUn(double valorUn) {
        this.valorUn = valorUn;
    }

    public float getValorRes() {
        return valorRes;
    }

    public void setValorRes(float valorRes) {
        this.valorRes = valorRes;
    }

    public float getTaxaMnt() {
        return taxaMnt;
    }

    public void setTaxaMnt(float taxaMnt) {
        this.taxaMnt = taxaMnt;
    }

    public Character getTipob() {
        return tipob;
    }

    public void setTipob(Character tipob) {
        this.tipob = tipob;
    }

    public Character getTipoAgro() {
        return tipoAgro;
    }

    public void setTipoAgro(Character tipoAgro) {
        this.tipoAgro = tipoAgro;
    }

    public Propriedade getPropId() {
        return propId;
    }

    public void setPropId(Propriedade propId) {
        this.propId = propId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bens)) {
            return false;
        }
        Bens other = (Bens) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.embrapa.gerruraltestebanco.model.Bens[ id=" + id + " ]";
    }
    
}
