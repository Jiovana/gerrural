/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.embrapa.agromanager.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gomes
 */
@Entity
@Table(name = "custos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Custos.findAll", query = "SELECT c FROM Custos c")
    , @NamedQuery(name = "Custos.findById", query = "SELECT c FROM Custos c WHERE c.id = :id")
    , @NamedQuery(name = "Custos.findByNome", query = "SELECT c FROM Custos c WHERE c.nome = :nome")
    , @NamedQuery(name = "Custos.findByDesc", query = "SELECT c FROM Custos c WHERE c.desc = :desc")
    , @NamedQuery(name = "Custos.findByTipo", query = "SELECT c FROM Custos c WHERE c.tipo = :tipo")
    , @NamedQuery(name = "Custos.findByValor", query = "SELECT c FROM Custos c WHERE c.valor = :valor")})
public class Custos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nome")
    private String nome;
    @Size(max = 100)
    @Column(name = "desc")
    private String desc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo")
    private Character tipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private double valor;
    @JoinColumn(name = "prop_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Propriedade propId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "custoId")
    private List<Custofunc> custofuncList;

    public Custos() {
    }

    public Custos(Integer id) {
        this.id = id;
    }

    public Custos(Integer id, String nome, Character tipo, double valor) {
        this.id = id;
        this.nome = nome;
        this.tipo = tipo;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Propriedade getPropId() {
        return propId;
    }

    public void setPropId(Propriedade propId) {
        this.propId = propId;
    }

    @XmlTransient
    public List<Custofunc> getCustofuncList() {
        return custofuncList;
    }

    public void setCustofuncList(List<Custofunc> custofuncList) {
        this.custofuncList = custofuncList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Custos)) {
            return false;
        }
        Custos other = (Custos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.embrapa.gerruraltestebanco.model.Custos[ id=" + id + " ]";
    }
    
}
