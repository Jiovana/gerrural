/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.embrapa.agromanager.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gomes
 */
@Entity
@Table(name = "pastagem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pastagem.findAll", query = "SELECT p FROM Pastagem p")
    , @NamedQuery(name = "Pastagem.findById", query = "SELECT p FROM Pastagem p WHERE p.id = :id")
    , @NamedQuery(name = "Pastagem.findByValArrend", query = "SELECT p FROM Pastagem p WHERE p.valArrend = :valArrend")
    , @NamedQuery(name = "Pastagem.findByValKgBoi", query = "SELECT p FROM Pastagem p WHERE p.valKgBoi = :valKgBoi")})
public class Pastagem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "val_arrend")
    private double valArrend;
    @Basic(optional = false)
    @NotNull
    @Column(name = "val_kg_boi")
    private double valKgBoi;
    @JoinColumn(name = "cam_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Campo camId;

    public Pastagem() {
    }

    public Pastagem(Integer id) {
        this.id = id;
    }

    public Pastagem(Integer id, double valArrend, double valKgBoi) {
        this.id = id;
        this.valArrend = valArrend;
        this.valKgBoi = valKgBoi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValArrend() {
        return valArrend;
    }

    public void setValArrend(double valArrend) {
        this.valArrend = valArrend;
    }

    public double getValKgBoi() {
        return valKgBoi;
    }

    public void setValKgBoi(double valKgBoi) {
        this.valKgBoi = valKgBoi;
    }

    public Campo getCamId() {
        return camId;
    }

    public void setCamId(Campo camId) {
        this.camId = camId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pastagem)) {
            return false;
        }
        Pastagem other = (Pastagem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.embrapa.gerruraltestebanco.model.Pastagem[ id=" + id + " ]";
    }
    
}
