/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.embrapa.agromanager.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gomes
 */
@Entity
@Table(name = "pecuaria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pecuaria.findAll", query = "SELECT p FROM Pecuaria p")
    , @NamedQuery(name = "Pecuaria.findById", query = "SELECT p FROM Pecuaria p WHERE p.id = :id")
    , @NamedQuery(name = "Pecuaria.findByDataIni", query = "SELECT p FROM Pecuaria p WHERE p.dataIni = :dataIni")
    , @NamedQuery(name = "Pecuaria.findByDataFim", query = "SELECT p FROM Pecuaria p WHERE p.dataFim = :dataFim")
    , @NamedQuery(name = "Pecuaria.findByRebTot", query = "SELECT p FROM Pecuaria p WHERE p.rebTot = :rebTot")
    , @NamedQuery(name = "Pecuaria.findByQtdVend", query = "SELECT p FROM Pecuaria p WHERE p.qtdVend = :qtdVend")
    , @NamedQuery(name = "Pecuaria.findByPesoMed", query = "SELECT p FROM Pecuaria p WHERE p.pesoMed = :pesoMed")
    , @NamedQuery(name = "Pecuaria.findByReceitBov", query = "SELECT p FROM Pecuaria p WHERE p.receitBov = :receitBov")
    , @NamedQuery(name = "Pecuaria.findByQtdIni", query = "SELECT p FROM Pecuaria p WHERE p.qtdIni = :qtdIni")
    , @NamedQuery(name = "Pecuaria.findByQtdFim", query = "SELECT p FROM Pecuaria p WHERE p.qtdFim = :qtdFim")})
public class Pecuaria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_ini")
    @Temporal(TemporalType.DATE)
    private Date dataIni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_fim")
    @Temporal(TemporalType.DATE)
    private Date dataFim;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reb_tot")
    private int rebTot;
    @Basic(optional = false)
    @NotNull
    @Column(name = "qtd_vend")
    private int qtdVend;
    @Basic(optional = false)
    @NotNull
    @Column(name = "peso_med")
    private double pesoMed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "receit_bov")
    private double receitBov;
    @Basic(optional = false)
    @NotNull
    @Column(name = "qtd_ini")
    private int qtdIni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "qtd_fim")
    private int qtdFim;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pecId")
    private List<Atividade> atividadeList;
    @JoinColumn(name = "prop_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Propriedade propId;

    public Pecuaria() {
    }

    public Pecuaria(Integer id) {
        this.id = id;
    }

    public Pecuaria(Integer id, Date dataIni, Date dataFim, int rebTot, int qtdVend, double pesoMed, double receitBov, int qtdIni, int qtdFim) {
        this.id = id;
        this.dataIni = dataIni;
        this.dataFim = dataFim;
        this.rebTot = rebTot;
        this.qtdVend = qtdVend;
        this.pesoMed = pesoMed;
        this.receitBov = receitBov;
        this.qtdIni = qtdIni;
        this.qtdFim = qtdFim;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataIni() {
        return dataIni;
    }

    public void setDataIni(Date dataIni) {
        this.dataIni = dataIni;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public int getRebTot() {
        return rebTot;
    }

    public void setRebTot(int rebTot) {
        this.rebTot = rebTot;
    }

    public int getQtdVend() {
        return qtdVend;
    }

    public void setQtdVend(int qtdVend) {
        this.qtdVend = qtdVend;
    }

    public double getPesoMed() {
        return pesoMed;
    }

    public void setPesoMed(double pesoMed) {
        this.pesoMed = pesoMed;
    }

    public double getReceitBov() {
        return receitBov;
    }

    public void setReceitBov(double receitBov) {
        this.receitBov = receitBov;
    }

    public int getQtdIni() {
        return qtdIni;
    }

    public void setQtdIni(int qtdIni) {
        this.qtdIni = qtdIni;
    }

    public int getQtdFim() {
        return qtdFim;
    }

    public void setQtdFim(int qtdFim) {
        this.qtdFim = qtdFim;
    }

    @XmlTransient
    public List<Atividade> getAtividadeList() {
        return atividadeList;
    }

    public void setAtividadeList(List<Atividade> atividadeList) {
        this.atividadeList = atividadeList;
    }

    public Propriedade getPropId() {
        return propId;
    }

    public void setPropId(Propriedade propId) {
        this.propId = propId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pecuaria)) {
            return false;
        }
        Pecuaria other = (Pecuaria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.embrapa.gerruraltestebanco.model.Pecuaria[ id=" + id + " ]";
    }
    
}
