/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.embrapa.agromanager.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gomes
 */
@Entity
@Table(name = "campo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Campo.findAll", query = "SELECT c FROM Campo c")
    , @NamedQuery(name = "Campo.findById", query = "SELECT c FROM Campo c WHERE c.id = :id")
    , @NamedQuery(name = "Campo.findByNome", query = "SELECT c FROM Campo c WHERE c.nome = :nome")
    , @NamedQuery(name = "Campo.findByDesc", query = "SELECT c FROM Campo c WHERE c.desc = :desc")
    , @NamedQuery(name = "Campo.findByHect", query = "SELECT c FROM Campo c WHERE c.hect = :hect")
    , @NamedQuery(name = "Campo.findByTipo", query = "SELECT c FROM Campo c WHERE c.tipo = :tipo")
    , @NamedQuery(name = "Campo.findByProp", query = "SELECT c FROM Campo c WHERE c.prop = :prop")
    , @NamedQuery(name = "Campo.findByDataIni", query = "SELECT c FROM Campo c WHERE c.dataIni = :dataIni")
    , @NamedQuery(name = "Campo.findByDataFim", query = "SELECT c FROM Campo c WHERE c.dataFim = :dataFim")})
public class Campo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nome")
    private String nome;
    @Size(max = 100)
    @Column(name = "desc")
    private String desc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hect")
    private double hect;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo")
    private Character tipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prop")
    private boolean prop;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_ini")
    @Temporal(TemporalType.DATE)
    private Date dataIni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_fim")
    @Temporal(TemporalType.DATE)
    private Date dataFim;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "camId")
    private List<Atividade> atividadeList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "camId")
    private List<Lavoura> lavouraList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "camId")
    private List<Pastagem> pastagemList;
    @JoinColumn(name = "prop_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Propriedade propId;

    public Campo() {
    }

    public Campo(Integer id) {
        this.id = id;
    }

    public Campo(Integer id, String nome, double hect, Character tipo, boolean prop, Date dataIni, Date dataFim) {
        this.id = id;
        this.nome = nome;
        this.hect = hect;
        this.tipo = tipo;
        this.prop = prop;
        this.dataIni = dataIni;
        this.dataFim = dataFim;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getHect() {
        return hect;
    }

    public void setHect(double hect) {
        this.hect = hect;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public boolean getProp() {
        return prop;
    }

    public void setProp(boolean prop) {
        this.prop = prop;
    }

    public Date getDataIni() {
        return dataIni;
    }

    public void setDataIni(Date dataIni) {
        this.dataIni = dataIni;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    @XmlTransient
    public List<Atividade> getAtividadeList() {
        return atividadeList;
    }

    public void setAtividadeList(List<Atividade> atividadeList) {
        this.atividadeList = atividadeList;
    }

    @XmlTransient
    public List<Lavoura> getLavouraList() {
        return lavouraList;
    }

    public void setLavouraList(List<Lavoura> lavouraList) {
        this.lavouraList = lavouraList;
    }

    @XmlTransient
    public List<Pastagem> getPastagemList() {
        return pastagemList;
    }

    public void setPastagemList(List<Pastagem> pastagemList) {
        this.pastagemList = pastagemList;
    }

    public Propriedade getPropId() {
        return propId;
    }

    public void setPropId(Propriedade propId) {
        this.propId = propId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Campo)) {
            return false;
        }
        Campo other = (Campo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.embrapa.gerruraltestebanco.model.Campo[ id=" + id + " ]";
    }
    
}
