/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.embrapa.agromanager.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gomes
 */
@Entity
@Table(name = "lavoura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lavoura.findAll", query = "SELECT l FROM Lavoura l")
    , @NamedQuery(name = "Lavoura.findById", query = "SELECT l FROM Lavoura l WHERE l.id = :id")
    , @NamedQuery(name = "Lavoura.findByQtdProdSc", query = "SELECT l FROM Lavoura l WHERE l.qtdProdSc = :qtdProdSc")
    , @NamedQuery(name = "Lavoura.findByPrecVendSc", query = "SELECT l FROM Lavoura l WHERE l.precVendSc = :precVendSc")
    , @NamedQuery(name = "Lavoura.findByArrendScHa", query = "SELECT l FROM Lavoura l WHERE l.arrendScHa = :arrendScHa")
    , @NamedQuery(name = "Lavoura.findBySecPropSc", query = "SELECT l FROM Lavoura l WHERE l.secPropSc = :secPropSc")
    , @NamedQuery(name = "Lavoura.findBySecTercSc", query = "SELECT l FROM Lavoura l WHERE l.secTercSc = :secTercSc")})
public class Lavoura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "qtd_prod_sc")
    private int qtdProdSc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prec_vend_sc")
    private double precVendSc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "arrend_sc_ha")
    private double arrendScHa;
    @Column(name = "sec_prop_sc")
    private Integer secPropSc;
    @Column(name = "sec_terc_sc")
    private Integer secTercSc;
    @JoinColumn(name = "cam_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Campo camId;

    public Lavoura() {
    }

    public Lavoura(Integer id) {
        this.id = id;
    }

    public Lavoura(Integer id, int qtdProdSc, double precVendSc, double arrendScHa) {
        this.id = id;
        this.qtdProdSc = qtdProdSc;
        this.precVendSc = precVendSc;
        this.arrendScHa = arrendScHa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQtdProdSc() {
        return qtdProdSc;
    }

    public void setQtdProdSc(int qtdProdSc) {
        this.qtdProdSc = qtdProdSc;
    }

    public double getPrecVendSc() {
        return precVendSc;
    }

    public void setPrecVendSc(double precVendSc) {
        this.precVendSc = precVendSc;
    }

    public double getArrendScHa() {
        return arrendScHa;
    }

    public void setArrendScHa(double arrendScHa) {
        this.arrendScHa = arrendScHa;
    }

    public Integer getSecPropSc() {
        return secPropSc;
    }

    public void setSecPropSc(Integer secPropSc) {
        this.secPropSc = secPropSc;
    }

    public Integer getSecTercSc() {
        return secTercSc;
    }

    public void setSecTercSc(Integer secTercSc) {
        this.secTercSc = secTercSc;
    }

    public Campo getCamId() {
        return camId;
    }

    public void setCamId(Campo camId) {
        this.camId = camId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lavoura)) {
            return false;
        }
        Lavoura other = (Lavoura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.embrapa.gerruraltestebanco.model.Lavoura[ id=" + id + " ]";
    }
    
}
