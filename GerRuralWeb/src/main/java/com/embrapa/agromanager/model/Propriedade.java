/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.embrapa.agromanager.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gomes
 */
@Entity
@Table(name = "propriedade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Propriedade.findAll", query = "SELECT p FROM Propriedade p")
    , @NamedQuery(name = "Propriedade.findById", query = "SELECT p FROM Propriedade p WHERE p.id = :id")
    , @NamedQuery(name = "Propriedade.findByNome", query = "SELECT p FROM Propriedade p WHERE p.nome = :nome")
    , @NamedQuery(name = "Propriedade.findByTipoPessoa", query = "SELECT p FROM Propriedade p WHERE p.tipoPessoa = :tipoPessoa")
    , @NamedQuery(name = "Propriedade.findByValorHect", query = "SELECT p FROM Propriedade p WHERE p.valorHect = :valorHect")
    , @NamedQuery(name = "Propriedade.findByLocal", query = "SELECT p FROM Propriedade p WHERE p.local = :local")})
public class Propriedade implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo_pessoa")
    private int tipoPessoa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor_hect")
    private double valorHect;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "local")
    private String local;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "propId")
    private List<Custos> custosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "propId")
    private List<Bens> bensList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "propId")
    private List<Pecuaria> pecuariaList;
    @JoinColumn(name = "prod_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario prodId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "propId")
    private List<Campo> campoList;

    public Propriedade() {
    }

    public Propriedade(Integer id) {
        this.id = id;
    }

    public Propriedade(Integer id, String nome, int tipoPessoa, double valorHect, String local) {
        this.id = id;
        this.nome = nome;
        this.tipoPessoa = tipoPessoa;
        this.valorHect = valorHect;
        this.local = local;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(int tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public double getValorHect() {
        return valorHect;
    }

    public void setValorHect(double valorHect) {
        this.valorHect = valorHect;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    @XmlTransient
    public List<Custos> getCustosList() {
        return custosList;
    }

    public void setCustosList(List<Custos> custosList) {
        this.custosList = custosList;
    }

    @XmlTransient
    public List<Bens> getBensList() {
        return bensList;
    }

    public void setBensList(List<Bens> bensList) {
        this.bensList = bensList;
    }

    @XmlTransient
    public List<Pecuaria> getPecuariaList() {
        return pecuariaList;
    }

    public void setPecuariaList(List<Pecuaria> pecuariaList) {
        this.pecuariaList = pecuariaList;
    }

    public Usuario getProdId() {
        return prodId;
    }

    public void setProdId(Usuario prodId) {
        this.prodId = prodId;
    }

    @XmlTransient
    public List<Campo> getCampoList() {
        return campoList;
    }

    public void setCampoList(List<Campo> campoList) {
        this.campoList = campoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Propriedade)) {
            return false;
        }
        Propriedade other = (Propriedade) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.embrapa.gerruraltestebanco.model.Propriedade[ id=" + id + " ]";
    }
    
}
